# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Aaron Griffin <aaron@archlinux.org>
# Contributor: Federico Quagliata (quaqo) <quaqo@despammed.com>
# Contributor: cdhotfire <cdhotfire@gmail.com>

pkgname=python-eyed3
_name=eyeD3
pkgver=0.9.7
pkgrel=5
epoch=1
pkgdesc="A Python module and program for processing information about mp3 files"
arch=('any')
url="http://eyed3.nicfit.net/"
license=('GPL-3.0-or-later')
depends=(
  'python-deprecation'
  'python-filetype'
  'python-magic'
  'python-setuptools'
)
checkdepends=(
  'python-factory-boy'
  'python-nose'
  'python-pytest'
)
optdepends=(
  'python-pillow: art plugin'
  'python-requests: art plugin'
  'python-ruamel-yaml: yamltag plugin'
  'python-pyaml: yamltag plugin'
  'python-pylast: lastfm plugin'
)
source=("$_name-$pkgver.tar.gz::https://github.com/nicfit/eyeD3/archive/refs/tags/v$pkgver.tar.gz")
sha256sums=('808f2d376b585ff13c35f614b970f3392c0f15de191c5a96c6b04532bf2217ec')
options=(!emptydirs)

build() {
  cd "$_name-$pkgver"
  python setup.py build
}

check() {
  cd "$_name-$pkgver"

  # Ignore tests which require:
  # - test data (test_classic_plugin.py, test_core.py, id3/test_frames.py,
  # id3_test_rva.py, test_issues.py)
  pytest --ignore=tests/{test_classic_plugin.py,test_core.py,id3/test_frames.py,test_jsonyaml_plugin.py,id3/test_rva.py,test_issues.py}
}

package() {
  cd "$_name-$pkgver"
  python setup.py install --root="$pkgdir" --optimize=1 --skip-build
}
